package com.jianshen.api.controller;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.ifast.api.util.StringUtils;
import com.ifast.api.util.result.ResultConstans;
import com.ifast.api.util.result.ResultUtils;
import com.jianshen.annotation.LoginUser;
import com.jianshen.op.domain.BaseuserDO;
import com.jianshen.op.domain.CourseDO;
import com.jianshen.op.service.CourseService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.annotation.Log;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:04:34 | zhangpl</small>
 */
@Controller
@RequestMapping("/api/course")
public class ApiCourseController extends AdminBaseController {
	@Autowired
	private CourseService courseService;



	/**
	*功能描述  预约课程
	*@Author  zhangpl
	*@Date 2018/11/1 15:59
	* @param  * @param request
	 * @param common
	 * @param teacherId
	 * @param studentId
	 * @param remarksName
	 * @param startDate
	 * @param endDate
	 * @param date
	* @return java.lang.Object
	*/
	@RequestMapping("insertCourse")
	@ResponseBody
	public Object insertCourse(HttpServletRequest request, @RequestParam("teacherId")String teacherId, @RequestParam("studentId")String studentId,
							   @RequestParam("remarksName")String remarksName, @RequestParam("startDate")String startDate,
							   @RequestParam("endDate")String endDate, @RequestParam("date")String date, @LoginUser BaseuserDO baseuserDO) {
		if(StringUtils.isEmptyString(teacherId) || StringUtils.isEmptyString(studentId) || StringUtils.isEmptyString(remarksName) || StringUtils.isEmptyString(startDate)
				|| StringUtils.isEmptyString(endDate) || StringUtils.isEmptyString(date)){
			return ResultUtils.result(ResultConstans.PARAMNULL);
		}



		startDate = date + " "+startDate+":00";
		endDate = date +" "+endDate+":00";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date endDate1 = null;
		Date startDate1 = null;
		try {
			endDate1 = format.parse(endDate);
			startDate1 = format.parse(startDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return ResultUtils.result(ResultConstans.EXCEPTION);
		}
		long nd = 1000 * 24 * 60 * 60;
		long nh = 1000 * 60 ;
		long diff = endDate1.getTime() - startDate1.getTime();
		if(diff<=0){
			return ResultUtils.result(ResultConstans.FAILED);
		}
		Long hour = diff % nd / nh;
		CourseDO course  = new CourseDO();
		course.setCourseDate(startDate1);
		course.setStartTime(startDate1);
		course.setEndTime(endDate1);
		course.setCourseTime(hour.intValue());
		if(baseuserDO.getId().equals(studentId)){  //说明是学生家客
			course.setState(0);
			course.setWhois(1);
		}else if(baseuserDO.getId().equals(teacherId)){ //老师加学生课程
			course.setState(1);
			course.setWhois(0);
		}
		course.setTeacherId(Integer.valueOf(teacherId));
		if(null != studentId && !"".equals(studentId) && !"null".equals(studentId)){
			course.setStudentId(Integer.valueOf(studentId));
		}
		List<CourseDO> courses = courseService.selectByTime(course);
		if(null == courses || courses.size() == 0){  //说明教练这段时间没课
			course.setCreatTime(new Date());
			courseService.insert(course);
			return ResultUtils.result(ResultConstans.SUCCESS);
		}
		//说明有课
		return ResultUtils.result(0,"此时间段不能预约.");

	}


}
