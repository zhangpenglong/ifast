package com.jianshen.api.controller;


import java.io.IOException;
import java.util.*;

import com.ifast.api.util.StringUtils;
import com.ifast.api.util.result.ResultConstans;
import com.ifast.api.util.result.ResultUtils;
import com.jianshen.annotation.LoginUser;
import com.jianshen.op.domain.BaseuserDO;
import com.jianshen.op.domain.FriendsDO;
import com.jianshen.op.service.FriendsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.annotation.Log;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * <pre>
 * 记录好友表
 * </pre>
 * <small> 2018-10-31 18:05:14 | zhangpl</small>
 */
@Controller
@RequestMapping("/api/friends")
public class ApiFriendsController extends AdminBaseController {
	@Autowired
	private FriendsService friendsService;

	@RequestMapping("selectBinding")
	@ResponseBody
	public Object selectBinding(HttpServletRequest request, @LoginUser BaseuserDO user) {
		Page<FriendsDO> friendsDOPage = friendsService.selectPage(getPage(FriendsDO.class), friendsService.convertToEntityWrapper("userId", user.getId(), "isValid", "1"));
		Map bodyMap = new HashMap();
		bodyMap.put("stList",friendsDOPage);
		return  ResultUtils.result(ResultConstans.SUCCESS,bodyMap);
	}



	/**
	*功能描述   绑定关系，
	*@Author  zhangpl
	*@Date 2018/11/1 15:57
	* @param  * @param request
	 * @param phoneNum
	 * @param remarks
	 * @param baseuserDO
	* @return java.lang.Object
	*/
	@RequestMapping("binding")
	@ResponseBody
	public Object binding(HttpServletRequest request,@RequestParam("phoneNum")String phoneNum,
						  @RequestParam("remarks")String remarks,@LoginUser BaseuserDO baseuserDO) {
		return friendsService.binding(phoneNum,remarks,baseuserDO);
	}


}
