package com.jianshen.api.controller;


import java.io.IOException;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.api.util.Constants;
import com.ifast.api.util.HttpUtil;
import com.ifast.api.util.JsonUtil;
import com.ifast.api.util.StringUtils;
import com.ifast.api.util.result.ResultConstans;
import com.ifast.api.util.result.ResultUtils;
import com.ifast.common.service.DictService;
import com.jianshen.annotation.IgnoreAuth;
import com.jianshen.annotation.LoginUser;
import com.jianshen.api.domain.FullUserInfo;
import com.jianshen.api.domain.UserInfo;
import com.jianshen.op.domain.BaseuserDO;
import com.jianshen.op.service.BaseuserService;
import com.jianshen.op.service.TokenService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.ifast.common.annotation.Log;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:03:13 | zhangpl</small>
 */
@Controller
@RequestMapping("/api/baseuser")
public class ApiBaseuserController extends AdminBaseController {

	private Logger logger = LoggerFactory.getLogger(getClass());


	@Autowired
	private BaseuserService baseuserService;

	@Autowired
	private TokenService tokenService;

	@Autowired
	private DictService dictService;
	

	@IgnoreAuth
	@RequestMapping("wxSession")
	@ResponseBody
	public Object login(HttpServletRequest request, @RequestParam(value = "code")String code){
		if(StringUtils.isEmptyString(code)){
			return ResultUtils.result(ResultConstans.PARAMNULL);
		}
		String url = Constants.JSCODE2SESSION;
		url += "?appId="+ Constants.APPID;
		url += "&secret="+Constants.SECRET;
		url += "&js_code="+code;
		url += "&grant_type="+Constants.GRANT_TYPE;
		try {
			String res = HttpUtil.get(url);
			if(StringUtils.isNotEmptyString(res)) {
				Map<String, String> resMap1 = JsonUtil.parseToMap(res);
				String unionid = resMap1.get("unionid");  //获取unionid，
				String openid = resMap1.get("openid");  //获取unionid，
				UserInfo userInfoObj = null;
				String session_key = resMap1.get("session_key");
				if(StringUtils.isNotEmptyString(openid) && StringUtils.isNotEmptyString(session_key)){
					BaseuserDO user = baseuserService.selectByOpenId(openid);
					String userInfo = request.getParameter("userInfo");
					if(StringUtils.isNotEmptyString(userInfo)){
						FullUserInfo fullUserInfo = JsonUtil.fromJson(userInfo, FullUserInfo.class);
						//验证用户信息完整性
						String sha1 = getSha1(fullUserInfo.getRawData() + session_key);
						if (!fullUserInfo.getSignature().equals(sha1)) {
							return ResultUtils.result(ResultConstans.LOGINFAIL);
						}
						userInfoObj = fullUserInfo.getUserInfo();
					}
					Map bodyMap = new HashMap();
					if(null != userInfoObj){
						if(null == user){
							user = new BaseuserDO();
						}
						user.setUserName(userInfoObj.getNickName());
						user.setWxNickName(userInfoObj.getNickName());
						user.setGender(userInfoObj.getGender());
						user.setProvince(userInfoObj.getProvince());
						user.setCity(userInfoObj.getCity());
						user.setAvatarUrl(userInfoObj.getAvatarUrl());
						user.setHeadUrl(userInfoObj.getAvatarUrl());
					}else{
						user.setUserName("微信用户"+session_key.substring(0,4));
						user.setHeadUrl(dictService.listByType("user_head_url").get(0).getValue());
					}
					user.setOpenId(openid);
					user.setSessionKey(session_key);
					if(null == user){
						baseuserService.insert(user);
						bodyMap.put("role","2");
					}else{
						baseuserService.updateById(user);
						if(null == user.getUserRole()){
							bodyMap.put("role","2");
						}else{
							bodyMap.put("role",user.getUserRole());
						}
					}
					bodyMap.put("userName",StringUtils.isEmptyString(user.getWxNickName()) ? user.getUserName() : user.getWxNickName());
					bodyMap.put("headUrl",StringUtils.isEmptyString(user.getAvatarUrl()) ? user.getHeadUrl() : user.getAvatarUrl());
					Map<String, Object> token = tokenService.createToken(user.getId());
					bodyMap.putAll(token);
					return ResultUtils.result(ResultConstans.SUCCESS,bodyMap);

				}else{
					return  ResultUtils.result(ResultConstans.RPCFAIL);
				}
			}
		} catch (IOException e) {
			logger.error("登录错误，",e);
			return ResultUtils.result(ResultConstans.FAILED);
		}
		return  ResultUtils.result(ResultConstans.FAILED);
	}





	@RequestMapping("setRole")
	@ResponseBody
	public Object setRole(HttpServletRequest request, @RequestParam("tapIndex")String tapIndex, @LoginUser BaseuserDO user) throws IOException{
		if(StringUtils.isEmptyString(tapIndex) ){
			return  ResultUtils.result(ResultConstans.PARAMNULL);
		}
		if(null == user || null != user.getUserRole()){
			return  ResultUtils.result(ResultConstans.PARAMNULL);
		}

		user.setUserRole(Integer.valueOf(tapIndex));
		baseuserService.updateById(user);
		return ResultUtils.result(ResultConstans.SUCCESS);
	}


	public static String getSha1(String str) {
		if (str == null || str.length() == 0) {
			return null;
		}
		char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f'};
		try {
			MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
			mdTemp.update(str.getBytes("UTF-8"));

			byte[] md = mdTemp.digest();
			int j = md.length;
			char buf[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
				buf[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(buf);
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}



}
