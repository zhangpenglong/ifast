package com.jianshen.interceptor;

import com.ifast.api.exception.ApiRRException;
import com.ifast.api.util.Constants;
import com.jianshen.annotation.IgnoreAuth;
import com.jianshen.op.domain.BaseuserDO;
import com.jianshen.op.domain.TokenDO;
import com.jianshen.op.service.BaseuserService;
import com.jianshen.op.service.TokenService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

/**
 * 权限(Token)验证
 *
 * @author tiankong
 * @email 2366207000@qq.com
 * @date 2017-03-23 15:38
 */
@Component
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {


	@Autowired
	private TokenService tokenService;

	@Autowired
    private BaseuserService baseuserService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    	
    	response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.addHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers","X-Requested-With,Host,User-Agent,Accept,Accept-Language,Accept-Encoding,Accept-Charset,Keep-Alive,Connection,Referer,Origin, Content-Type,authorization");
		
    	//String requestPath = getRequestPath(request);// 用户访问的资源地址
    	/*if(requestPath.contains("swagger") || requestPath.contains("api-docs")){
			return true;
		}*/
    	
        IgnoreAuth annotation;
        if (handler instanceof HandlerMethod) {
            annotation = ((HandlerMethod) handler).getMethodAnnotation(IgnoreAuth.class);
        } else {
            return true;
        }

        //如果有@IgnoreAuth注解，则不验证token
        if (annotation != null) {
            return true;
        }

        //从header中获取token
        String token = request.getHeader(Constants.LOGIN_TOKEN_KEY);
        //如果header中不存在token，则从参数中获取token
        if (StringUtils.isBlank(token)) {
            token = request.getHeader(Constants.LOGIN_TOKEN_KEY);
        }

        //token为空
        if (StringUtils.isBlank(token)) {
            throw new ApiRRException("请先登录", 401);
        }

        //查询token信息
        TokenDO tokenEntity = tokenService.queryByToken(token);
        if (tokenEntity == null || tokenEntity.getExpireTime().getTime() < System.currentTimeMillis()) {
            throw new ApiRRException("token失效，请重新登录", 401);
        }

        //设置userId到request里，后续根据userId，获取用户信息
        BaseuserDO oneByKv = baseuserService.selectById(tokenEntity.getUserId());
        request.setAttribute(Constants.LOGIN_USER_KEY, oneByKv);

        return true;
    }
    
    /**
	 * 截取访问路径
	 * 
	 * @param request
	 * @return
	 */
	public static String getRequestPath(HttpServletRequest request) {
		ServletContext servletContext=request.getServletContext();
		//使用ServletContext获取指定目录下所有资源路径

		Set<String> paths=servletContext.getResourcePaths("/META-INF/resources");

		//System.out.println(paths);//[/WEB-INF/lib, /WEB-INF/classes,/WEB-INF/web.xml]
		String requestPath = request.getRequestURI();
		// 去掉其他参数
		if (requestPath.indexOf("&") > -1) {
			requestPath = requestPath.substring(0, requestPath.indexOf("&"));
		}
		// 去掉jsessionid参数
		if(requestPath.indexOf(";") > -1){
			requestPath = requestPath.substring(0, requestPath.indexOf(";"));
		}
		// 去掉项目路径
		requestPath = requestPath.substring(request.getContextPath().length() + 1);
		return requestPath;
	}
}
