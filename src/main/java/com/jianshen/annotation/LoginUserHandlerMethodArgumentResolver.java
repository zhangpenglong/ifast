package com.jianshen.annotation;

import com.ifast.api.util.Constants;
import com.ifast.api.util.StringUtils;
import com.ifast.common.config.Constant;
import com.jianshen.op.domain.BaseuserDO;
import com.jianshen.op.domain.TokenDO;
import com.jianshen.op.service.BaseuserService;
import com.jianshen.op.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * 有@LoginUser注解的方法参数，注入当前登录用户
 *
 * @author tiankong
 * @email 2366207000@qq.com
 * @date 2017-03-23 22:02
 */
public class LoginUserHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {
    @Autowired
    private BaseuserService baseuserService;

    @Autowired
    private TokenService tokenService;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(BaseuserDO.class) && parameter.hasParameterAnnotation(LoginUser.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer container,
                                  NativeWebRequest request, WebDataBinderFactory factory) throws Exception {
        Object attribute = request.getAttribute(Constants.LOGIN_USER_KEY, RequestAttributes.SCOPE_REQUEST);
        return attribute;
    }
}
