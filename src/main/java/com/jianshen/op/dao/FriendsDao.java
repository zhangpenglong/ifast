package com.jianshen.op.dao;

import com.ifast.common.base.BaseDao;
import com.jianshen.op.domain.FriendsDO;

/**
 * 
 * <pre>
 * 记录好友表
 * </pre>
 * <small> 2018-10-31 18:05:14 | zhangpl</small>
 */
public interface FriendsDao extends BaseDao<FriendsDO> {

}
