package com.jianshen.op.dao;

import com.ifast.common.base.BaseDao;
import com.jianshen.op.domain.UerTeacherDO;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:05:28 | zhangpl</small>
 */
public interface UerTeacherDao extends BaseDao<UerTeacherDO> {

}
