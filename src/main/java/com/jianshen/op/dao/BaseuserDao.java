package com.jianshen.op.dao;

import com.ifast.common.base.BaseDao;
import com.jianshen.op.domain.BaseuserDO;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:03:13 | zhangpl</small>
 */
public interface BaseuserDao extends BaseDao<BaseuserDO> {

    /**
     *功能描述   根据openId查询用户
     *@Author  zhangpl
     *@Date 2018/10/31 19:06
     * @param  * @param openid
     * @return com.jianshen.op.domain.BaseuserDO
     */
    BaseuserDO selectByOpenId(String openid);

}
