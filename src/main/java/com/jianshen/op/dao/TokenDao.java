package com.jianshen.op.dao;

import com.ifast.common.base.BaseDao;
import com.jianshen.op.domain.TokenDO;

/**
 * 
 * <pre>
 * 用户Token
 * </pre>
 * <small> 2018-11-01 10:15:36 | zhangpl</small>
 */
public interface TokenDao extends BaseDao<TokenDO> {


    /**
    *功能描述   根据userId查询token是否存在
    *@Author  zhangpl
    *@Date 2018/11/1 10:22
    * @param  * @param userId
    * @return com.jianshen.op.domain.TokenDO
    */
    TokenDO queryByUserId(Long userId);


    /**
    *功能描述  根据token查询
    *@Author  zhangpl
    *@Date 2018/11/1 10:27
    * @param  * @param token
    * @return com.jianshen.op.domain.TokenDO
    */
    TokenDO queryByToken(String token);

}
