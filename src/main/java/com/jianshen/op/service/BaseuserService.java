package com.jianshen.op.service;

import com.ifast.common.base.CoreService;
import com.jianshen.op.domain.BaseuserDO;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:03:13 | zhangpl</small>
 */
public interface BaseuserService extends CoreService<BaseuserDO> {



    /**
    *功能描述   根据openId查询用户
    *@Author  zhangpl
    *@Date 2018/10/31 19:06
    * @param  * @param openid
    * @return com.jianshen.op.domain.BaseuserDO
    */
    BaseuserDO selectByOpenId(String openid);


    /**
    *功能描述  根据手机号查询用户
    *@Author  zhangpl
    *@Date 2018/11/1 13:47
    * @param  * @param phoneNum
    * @return com.jianshen.op.domain.BaseuserDO
    */
    BaseuserDO selectByPhone(String phoneNum);
    
}
