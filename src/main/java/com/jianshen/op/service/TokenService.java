package com.jianshen.op.service;

import com.ifast.common.base.CoreService;
import com.jianshen.op.domain.TokenDO;

import java.util.Map;

/**
 * 
 * <pre>
 * 用户Token
 * </pre>
 * <small> 2018-11-01 10:15:36 | zhangpl</small>
 */
public interface TokenService extends CoreService<TokenDO> {

    /**
     *功能描述   根据userId查询token是否存在
     *@Author  zhangpl
     *@Date 2018/11/1 10:22
     * @param  * @param userId
     * @return com.jianshen.op.domain.TokenDO
     */
    TokenDO queryByUserId(Long userId);


    /**
     *功能描述  根据token查询
     *@Author  zhangpl
     *@Date 2018/11/1 10:27
     * @param  * @param token
     * @return com.jianshen.op.domain.TokenDO
     */
    TokenDO queryByToken(String token);


    /**
     *功能描述  在登录时调用，返回一个token
     *@Author  zhangpl
     *@Date 2018/11/1 10:30
     * @param  * @param userId
     * @return java.util.Map<java.lang.String,java.lang.Object>
     */
    Map<String, Object> createToken(long userId);

}
