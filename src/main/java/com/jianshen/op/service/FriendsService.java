package com.jianshen.op.service;

import com.ifast.api.util.result.ResultUtils;
import com.ifast.common.base.CoreService;
import com.jianshen.op.domain.BaseuserDO;
import com.jianshen.op.domain.FriendsDO;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 
 * <pre>
 * 记录好友表
 * </pre>
 * <small> 2018-10-31 18:05:14 | zhangpl</small>
 */
public interface FriendsService extends CoreService<FriendsDO> {

    ResultUtils binding(String phoneNum,String remarks,BaseuserDO baseuserDO);
    
}
