package com.jianshen.op.service;

import com.ifast.common.base.CoreService;
import com.jianshen.op.domain.UerTeacherDO;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:05:28 | zhangpl</small>
 */
public interface UerTeacherService extends CoreService<UerTeacherDO> {
    
}
