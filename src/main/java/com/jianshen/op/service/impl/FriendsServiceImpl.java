package com.jianshen.op.service.impl;

import com.ifast.api.util.StringUtils;
import com.ifast.api.util.result.ResultConstans;
import com.ifast.api.util.result.ResultUtils;
import com.ifast.common.base.CoreServiceImpl;
import com.jianshen.op.dao.FriendsDao;
import com.jianshen.op.domain.BaseuserDO;
import com.jianshen.op.domain.FriendsDO;
import com.jianshen.op.service.BaseuserService;
import com.jianshen.op.service.FriendsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * <pre>
 * 记录好友表
 * </pre>
 * <small> 2018-10-31 18:05:14 | zhangpl</small>
 */
@Service
public class FriendsServiceImpl extends CoreServiceImpl<FriendsDao, FriendsDO> implements FriendsService {

    @Autowired
    private BaseuserService baseuserService;

    /**
    *功能描述  绑定两个人的关系
    *@Author  zhangpl
    *@Date 2018/11/1 15:57
    * @param  * @param phoneNum
     * @param remarks
     * @param baseuserDO
    * @return com.ifast.api.util.result.ResultUtils
    */
    @Override
    public ResultUtils binding(String phoneNum,String remarks,BaseuserDO baseuserDO) {
        if(StringUtils.isEmptyString(phoneNum)  || StringUtils.isEmptyString(remarks)){
            return ResultUtils.result(ResultConstans.PARAMNULL);
        }
        BaseuserDO user = baseuserService.selectByPhone(phoneNum);
        if (null == user) {
            return ResultUtils.result(0,"用户不存在。");
        }
        if (null == user.getUserRole() || user.getUserRole() == baseuserDO.getUserRole()) {
            return ResultUtils.result(0,"此账号不能添加。");
        }
        FriendsDO friendsDO = this.selectOne(convertToEntityWrapper("userId", baseuserDO.getId(), "friendsId", user.getId()));
        if(null == friendsDO){  //说明没有绑定关系，
            friendsDO.setUserId(baseuserDO.getId());
            friendsDO.setFriendsId(user.getId());
            friendsDO.setRemarksname(remarks);
            insert(friendsDO);
            return ResultUtils.result(ResultConstans.SUCCESS);
        }

        return ResultUtils.result(ResultConstans.SUCCESS);
    }
}
