package com.jianshen.op.service.impl;

import com.ifast.common.service.DictService;
import com.jianshen.op.dao.TokenDao;
import com.jianshen.op.domain.TokenDO;
import com.jianshen.op.service.TokenService;
import com.jianshen.util.CharUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifast.common.base.CoreServiceImpl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * <pre>
 * 用户Token
 * </pre>
 * <small> 2018-11-01 10:15:36 | zhangpl</small>
 */
@Service
public class TokenServiceImpl extends CoreServiceImpl<TokenDao, TokenDO> implements TokenService {

    @Autowired
    private DictService dictService;

    /**
    *功能描述 根据userId查询token
    *@Author  zhangpl
    *@Date 2018/11/1 10:30
    * @param  * @param userId
    * @return com.jianshen.op.domain.TokenDO
    */
    @Override
    public TokenDO queryByUserId(Long userId) {
        return baseMapper.queryByUserId(userId);
    }

    /**
    *功能描述  根据token查询
    *@Author  zhangpl
    *@Date 2018/11/1 10:30
    * @param  * @param token
    * @return com.jianshen.op.domain.TokenDO
    */
    @Override
    public TokenDO queryByToken(String token) {
        return baseMapper.queryByToken(token);
    }


    /**
    *功能描述  在登录时调用，返回一个token
    *@Author  zhangpl
    *@Date 2018/11/1 10:30
    * @param  * @param userId
    * @return java.util.Map<java.lang.String,java.lang.Object>
    */
    @Override
    public Map<String, Object> createToken(long userId) {

        //过期时间
        Integer expire = Integer.valueOf(dictService.listByType("token_expire_time").get(0).getValue());
        //生成一个token
        String token = CharUtil.getRandomString(32);
        //当前时间
        Date now = new Date();

        //过期时间
        Date expireTime = new Date(now.getTime() + expire * 1000);

        //判断是否生成过token
        TokenDO tokenEntity = queryByUserId(userId);
        if (tokenEntity == null) {
            tokenEntity = new TokenDO();
            tokenEntity.setUserId(userId);
            tokenEntity.setToken(token);
            tokenEntity.setUpdateTime(now);
            tokenEntity.setExpireTime(expireTime);
            //保存token
            insert(tokenEntity);
        } else {
            tokenEntity.setToken(token);
            tokenEntity.setUpdateTime(now);
            tokenEntity.setExpireTime(expireTime);
            //更新token
            updateById(tokenEntity);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("expire", expire);

        return map;
    }

}
