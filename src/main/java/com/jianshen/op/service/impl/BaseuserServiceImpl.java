package com.jianshen.op.service.impl;

import com.ifast.common.base.CoreServiceImpl;
import com.jianshen.op.dao.BaseuserDao;
import com.jianshen.op.domain.BaseuserDO;
import com.jianshen.op.service.BaseuserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:03:13 | zhangpl</small>
 */
@Service
public class BaseuserServiceImpl extends CoreServiceImpl<BaseuserDao, BaseuserDO> implements BaseuserService {


    @Override
    public BaseuserDO selectByOpenId(String openid) {
        return baseMapper.selectByOpenId(openid);
    }

    @Override
    public BaseuserDO selectByPhone(String phoneNum) {
        return selectOne(this.convertToEntityWrapper("phoneNum",phoneNum));
    }
}
