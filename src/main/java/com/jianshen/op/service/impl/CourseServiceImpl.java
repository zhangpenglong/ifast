package com.jianshen.op.service.impl;

import com.ifast.common.base.CoreServiceImpl;
import com.jianshen.op.dao.CourseDao;
import com.jianshen.op.domain.CourseDO;
import com.jianshen.op.service.CourseService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:04:34 | zhangpl</small>
 */
@Service
public class CourseServiceImpl extends CoreServiceImpl<CourseDao, CourseDO> implements CourseService {


    /**
     *功能描述  查询此时间段该人有没有教练
     *@Author  zhangpl
     *@Date 2018/11/1 16:03
     * @param  * @param courseDO
     * @return com.jianshen.op.domain.CourseDO
     */
    @Override
    public List<CourseDO> selectByTime(CourseDO courseDO){
      return baseMapper.selectByTime(courseDO);
    }

}
