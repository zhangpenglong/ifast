package com.jianshen.op.service.impl;

import com.ifast.common.base.CoreServiceImpl;
import com.jianshen.op.dao.UerTeacherDao;
import com.jianshen.op.domain.UerTeacherDO;
import com.jianshen.op.service.UerTeacherService;
import org.springframework.stereotype.Service;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:05:28 | zhangpl</small>
 */
@Service
public class UerTeacherServiceImpl extends CoreServiceImpl<UerTeacherDao, UerTeacherDO> implements UerTeacherService {

}
