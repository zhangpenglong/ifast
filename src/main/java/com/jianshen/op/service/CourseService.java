package com.jianshen.op.service;

import com.ifast.common.base.CoreService;
import com.jianshen.op.domain.CourseDO;

import java.util.List;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:04:34 | zhangpl</small>
 */
public interface CourseService extends CoreService<CourseDO> {


    /**
     *功能描述  查询此时间段该人有没有教练
     *@Author  zhangpl
     *@Date 2018/11/1 16:03
     * @param  * @param courseDO
     * @return com.jianshen.op.domain.CourseDO
     */
    List<CourseDO> selectByTime(CourseDO courseDO);
    
}
