package com.jianshen.op.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.annotation.Log;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;
import com.jianshen.op.domain.UerTeacherDO;
import com.jianshen.op.service.UerTeacherService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:05:28 | zhangpl</small>
 */
@Controller
@RequestMapping("/jianshen/uerTeacher")
public class UerTeacherController extends AdminBaseController {
	@Autowired
	private UerTeacherService uerTeacherService;
	
	@GetMapping()
	@RequiresPermissions("jianshen:uerTeacher:uerTeacher")
	String UerTeacher(){
	    return "jianshen/uerTeacher/uerTeacher";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("jianshen:uerTeacher:uerTeacher")
	public Result<Page<UerTeacherDO>> list(UerTeacherDO uerTeacherDTO){
        Wrapper<UerTeacherDO> wrapper = new EntityWrapper<UerTeacherDO>().orderBy("id", false);
        Page<UerTeacherDO> page = uerTeacherService.selectPage(getPage(UerTeacherDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("jianshen:uerTeacher:add")
	String add(){
	    return "jianshen/uerTeacher/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("jianshen:uerTeacher:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		UerTeacherDO uerTeacher = uerTeacherService.selectById(id);
		model.addAttribute("uerTeacher", uerTeacher);
	    return "jianshen/uerTeacher/edit";
	}
	
	@Log("添加")
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("jianshen:uerTeacher:add")
	public Result<String> save( UerTeacherDO uerTeacher){
		uerTeacherService.insert(uerTeacher);
        return Result.ok();
	}
	
	@Log("修改")
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("jianshen:uerTeacher:edit")
	public Result<String>  update( UerTeacherDO uerTeacher){
		boolean update = uerTeacherService.updateById(uerTeacher);
		return update ? Result.ok() : Result.fail();
	}
	
	@Log("删除")
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("jianshen:uerTeacher:remove")
	public Result<String>  remove( Integer id){
		uerTeacherService.deleteById(id);
        return Result.ok();
	}
	
	@Log("批量删除")
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("jianshen:uerTeacher:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") Integer[] ids){
		uerTeacherService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
