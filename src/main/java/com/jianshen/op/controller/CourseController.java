package com.jianshen.op.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.annotation.Log;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;
import com.jianshen.op.domain.CourseDO;
import com.jianshen.op.service.CourseService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:04:34 | zhangpl</small>
 */
@Controller
@RequestMapping("/jianshen/course")
public class CourseController extends AdminBaseController {
	@Autowired
	private CourseService courseService;
	
	@GetMapping()
	@RequiresPermissions("jianshen:course:course")
	String Course(){
	    return "jianshen/course/course";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("jianshen:course:course")
	public Result<Page<CourseDO>> list(CourseDO courseDTO){
        Wrapper<CourseDO> wrapper = new EntityWrapper<CourseDO>().orderBy("id", false);
        Page<CourseDO> page = courseService.selectPage(getPage(CourseDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("jianshen:course:add")
	String add(){
	    return "jianshen/course/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("jianshen:course:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		CourseDO course = courseService.selectById(id);
		model.addAttribute("course", course);
	    return "jianshen/course/edit";
	}
	
	@Log("添加")
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("jianshen:course:add")
	public Result<String> save( CourseDO course){
		courseService.insert(course);
        return Result.ok();
	}
	
	@Log("修改")
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("jianshen:course:edit")
	public Result<String>  update( CourseDO course){
		boolean update = courseService.updateById(course);
		return update ? Result.ok() : Result.fail();
	}
	
	@Log("删除")
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("jianshen:course:remove")
	public Result<String>  remove( Integer id){
		courseService.deleteById(id);
        return Result.ok();
	}
	
	@Log("批量删除")
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("jianshen:course:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") Integer[] ids){
		courseService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
