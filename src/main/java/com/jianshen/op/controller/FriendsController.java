package com.jianshen.op.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.annotation.Log;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;
import com.jianshen.op.domain.FriendsDO;
import com.jianshen.op.service.FriendsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 
 * <pre>
 * 记录好友表
 * </pre>
 * <small> 2018-10-31 18:05:14 | zhangpl</small>
 */
@Controller
@RequestMapping("/jianshen/friends")
public class FriendsController extends AdminBaseController {
	@Autowired
	private FriendsService friendsService;
	
	@GetMapping()
	@RequiresPermissions("jianshen:friends:friends")
	String Friends(){
	    return "jianshen/friends/friends";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("jianshen:friends:friends")
	public Result<Page<FriendsDO>> list(FriendsDO friendsDTO){
        Wrapper<FriendsDO> wrapper = new EntityWrapper<FriendsDO>().orderBy("id", false);
        Page<FriendsDO> page = friendsService.selectPage(getPage(FriendsDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("jianshen:friends:add")
	String add(){
	    return "jianshen/friends/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("jianshen:friends:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		FriendsDO friends = friendsService.selectById(id);
		model.addAttribute("friends", friends);
	    return "jianshen/friends/edit";
	}
	
	@Log("添加记录好友表")
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("jianshen:friends:add")
	public Result<String> save( FriendsDO friends){
		friendsService.insert(friends);
        return Result.ok();
	}
	
	@Log("修改记录好友表")
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("jianshen:friends:edit")
	public Result<String>  update( FriendsDO friends){
		boolean update = friendsService.updateById(friends);
		return update ? Result.ok() : Result.fail();
	}
	
	@Log("删除记录好友表")
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("jianshen:friends:remove")
	public Result<String>  remove( Integer id){
		friendsService.deleteById(id);
        return Result.ok();
	}
	
	@Log("批量删除记录好友表")
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("jianshen:friends:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") Integer[] ids){
		friendsService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
