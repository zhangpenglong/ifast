package com.jianshen.op.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.annotation.Log;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;
import com.jianshen.op.domain.BaseuserDO;
import com.jianshen.op.service.BaseuserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:03:13 | zhangpl</small>
 */
@Controller
@RequestMapping("/jianshen/baseuser")
public class BaseuserController extends AdminBaseController {
	@Autowired
	private BaseuserService baseuserService;
	
	@GetMapping()
	@RequiresPermissions("jianshen:baseuser:baseuser")
	String Baseuser(){
	    return "jianshen/baseuser/baseuser";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("jianshen:baseuser:baseuser")
	public Result<Page<BaseuserDO>> list(BaseuserDO baseuserDTO){
        Wrapper<BaseuserDO> wrapper = new EntityWrapper<BaseuserDO>().orderBy("id", false);
        Page<BaseuserDO> page = baseuserService.selectPage(getPage(BaseuserDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("jianshen:baseuser:add")
	String add(){
	    return "jianshen/baseuser/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("jianshen:baseuser:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		BaseuserDO baseuser = baseuserService.selectById(id);
		model.addAttribute("baseuser", baseuser);
	    return "jianshen/baseuser/edit";
	}
	
	@Log("添加")
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("jianshen:baseuser:add")
	public Result<String> save( BaseuserDO baseuser){
		baseuserService.insert(baseuser);
        return Result.ok();
	}
	
	@Log("修改")
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("jianshen:baseuser:edit")
	public Result<String>  update( BaseuserDO baseuser){
		boolean update = baseuserService.updateById(baseuser);
		return update ? Result.ok() : Result.fail();
	}
	
	@Log("删除")
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("jianshen:baseuser:remove")
	public Result<String>  remove( Integer id){
		baseuserService.deleteById(id);
        return Result.ok();
	}
	
	@Log("批量删除")
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("jianshen:baseuser:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") Integer[] ids){
		baseuserService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
