package com.jianshen.op.controller;


import java.util.Arrays;

import com.jianshen.op.domain.TokenDO;
import com.jianshen.op.service.TokenService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.annotation.Log;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;

/**
 * 
 * <pre>
 * 用户Token
 * </pre>
 * <small> 2018-11-01 10:15:36 | zhangpl</small>
 */
@Controller
@RequestMapping("/jianshen/token")
public class TokenController extends AdminBaseController {
	@Autowired
	private TokenService tokenService;
	
	@GetMapping()
	@RequiresPermissions("jianshen:token:token")
	String Token(){
	    return "jianshen/token/token";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("jianshen:token:token")
	public Result<Page<TokenDO>> list(TokenDO tokenDTO){
        Wrapper<TokenDO> wrapper = new EntityWrapper<TokenDO>().orderBy("id", false);
        Page<TokenDO> page = tokenService.selectPage(getPage(TokenDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("jianshen:token:add")
	String add(){
	    return "jianshen/token/add";
	}

	@GetMapping("/edit/{userId}")
	@RequiresPermissions("jianshen:token:edit")
	String edit(@PathVariable("userId") Long userId,Model model){
		TokenDO token = tokenService.selectById(userId);
		model.addAttribute("token", token);
	    return "jianshen/token/edit";
	}
	
	@Log("添加用户Token")
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("jianshen:token:add")
	public Result<String> save( TokenDO token){
		tokenService.insert(token);
        return Result.ok();
	}
	
	@Log("修改用户Token")
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("jianshen:token:edit")
	public Result<String>  update( TokenDO token){
		boolean update = tokenService.updateById(token);
		return update ? Result.ok() : Result.fail();
	}
	
	@Log("删除用户Token")
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("jianshen:token:remove")
	public Result<String>  remove( Long userId){
		tokenService.deleteById(userId);
        return Result.ok();
	}
	
	@Log("批量删除用户Token")
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("jianshen:token:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") Long[] userIds){
		tokenService.deleteBatchIds(Arrays.asList(userIds));
		return Result.ok();
	}
	
}
