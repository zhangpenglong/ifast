package com.jianshen.op.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import com.ifast.common.base.BaseDO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * <pre>
 * 用户Token
 * </pre>
 * <small> 2018-11-01 10:15:36 | zhangpl</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("jianshen_token")
public class TokenDO implements Serializable {
	@TableId
	private Long id;

	private Long userId;

    /** token */
    private String token;

    /** 过期时间 */
    private Date expireTime;

    /** 更新时间 */
    private Date updateTime;

}
