package com.jianshen.op.domain;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * <pre>
 * 记录好友表
 * </pre>
 * <small> 2018-10-31 18:05:14 | zhangpl</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("jianshen_friends")
public class FriendsDO implements Serializable {
	@TableId
	private Long id;

    /** 用户Id */
    private Long userId;

    /** 好友ID */
    private Long friendsId;

    /** 备注名 */
    private String remarksname;

    /**  */
    private Integer isValid;

    /**  */
    private Date createDate;

    /**  */
    private Date updateDate;

}
