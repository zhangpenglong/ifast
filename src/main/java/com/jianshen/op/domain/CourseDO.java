package com.jianshen.op.domain;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:04:34 | zhangpl</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("jianshen_course")
public class CourseDO implements Serializable {
	@TableId
	private Long id;

    /** 课程名称 */
    private String courseName;

    /** 课程日期 */
    private Date courseDate;

    /** 课程开始时间 */
    private Date startTime;

    /** 课程结束时间 */
    private Date endTime;

    /** 课程时长 */
    private Integer courseTime;

    /** 0,等待教练确认，1，等待学员确认，2，教练拒绝，3，学员拒绝，4，教练取消，5学员取消，6，已结束，7，正在进行，8，未开始,9超时 */
    private Integer state;

    /** 拒绝理由 */
    private String refuseExplain;

    /** 拒绝时间 */
    private Date refuseTime;

    /** 取消理由 */
    private String cancelExplain;

    /** 取消时间 */
    private Date cancelTime;

    /** 教练 */
    private Integer teacherId;

    /** 学员 */
    private Integer studentId;

    /** 由谁发起，0，教练。1，学员 */
    private Integer whois;

    /** 发起时间 */
    private Date creatTime;

    /**  */
    private Integer isValid;

    /**  */
    private Date createDate;

    /**  */
    private Date updateDate;

}
