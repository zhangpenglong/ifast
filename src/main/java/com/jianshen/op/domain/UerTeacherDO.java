package com.jianshen.op.domain;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:05:28 | zhangpl</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("jianshen_uer_teacher")
public class UerTeacherDO implements Serializable {
	@TableId
	private Long id;

    /**  */
    private Integer userId;

    /** 证书 */
    private String certificate;

    /** 教练星级 */
    private String star;

    /** 健身房 */
    private Integer gymId;

    /** 特长 */
    private String specialty;

    /**  */
    private Integer isValid;

    /**  */
    private Date createDate;

    /**  */
    private Date updateDate;

}
