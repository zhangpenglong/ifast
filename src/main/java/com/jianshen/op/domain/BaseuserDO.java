package com.jianshen.op.domain;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * <pre>
 * 
 * </pre>
 * <small> 2018-10-31 18:03:13 | zhangpl</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("jianshen_baseuser")
public class BaseuserDO implements Serializable {
	@TableId
	private Long id;

    /** 用户名（唯一） */
    private String userName;

    /** 微信名称 */
    private String wxNickName;

    /** 性别1，男   2女   0未知 */
    private Integer gender;

    /** 国家取自微信 */
    private String country;

    /** 省份微信 */
    private String province;

    /** 城市   微信 */
    private String city;

    /** 微信头像 */
    private String avatarUrl;

    /** 头像 */
    private String headUrl;

    /** 用户角色  0，学员。1教练 */
    private Integer userRole;

    /** 手机号 */
    private String phoneNum;

    /** 微信唯一表示 */
    private String openId;

    /** 微信session */
    private String sessionKey;

    /** 目标体重 */
    private Double willWeight;

    /** 体重   KG */
    private Double weight;

    /** 身高 */
    private Double  height;

    /** 生日 */
    private Date birthday;

    /** 备用字段1 */
    private String backup1;

    /**  */
    private String backup2;

    /**  */
    private String backup3;

    /** 是否有效 */
    private Integer isValid;

    /**  */
    private Date createDate;

    /**  */
    private Date updateDate;

}
