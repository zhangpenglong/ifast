package com.ifast.api.util.result;

/**
 * @date 2018年10月9日11:09:30
 * Created by zhangpl
 * 课件系统统一返回结果类。
 */
public class ResultUtils {

    public String code;
    public String msg;

    public ResultUtils() {}


    public ResultUtils(int code, String msg){
        this.code = code+"";
        this.msg = msg;
    }

    public ResultUtils(ResultConstans resultConstans){
        this.code = resultConstans.getCode()+"";
        this.msg = resultConstans.getMsg();
    }


    public static ResultUtils result(ResultConstans cwResultConstans, Object body){
        return new BodyResult(new BaseResult(cwResultConstans.code,cwResultConstans.msg),body);
    }
    public static ResultUtils result(int code, String msg, Object body){
        return new BodyResult(new BaseResult(code,msg),body);
    }

    public static ResultUtils result(ResultConstans cwResultConstans){
        return new ResultUtils(cwResultConstans);
    }

    public static ResultUtils result(int code, String msg){
        return new ResultUtils(code,msg);
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
