package com.ifast.api.exception;

import com.ifast.api.util.result.ResultConstans;
import com.ifast.api.util.result.ResultUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

/**
 * 异常处理器
 *
 * @author tiankong
 * @email 2366207000@qq.com
 * @date 2016年10月27日 下午10:16:19
 */
@RestControllerAdvice(value = {"com.jianshen"})
public class RRExceptionHandler {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(ApiRRException.class)
    public Object handleApiRRException(ApiRRException e) {
        HashMap result = new HashMap();
        result.put("code", e.getErrno());
        result.put("msg", e.getErrmsg());
        return result;
    }


    @ExceptionHandler(Exception.class)
    public Object handleException(Exception e) {
        logger.error(e.getMessage(), e);
        return ResultUtils.result(ResultConstans.EXCEPTION);
    }
}
